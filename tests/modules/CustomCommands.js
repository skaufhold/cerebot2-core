
import sinon from 'sinon';
import expect from 'expect';
import tmi from 'tmi.js';

import { Command } from 'cerebot2-common';

import CustomCommands from '../../src/modules/CustomCommands';

describe('Custom commands', function() {
    beforeEach(() => {
        this.client = new tmi.client();
        this.linkBlockerModule = new CustomCommands(this.client);
    });

    it('creates a command', (done) => {
        sinon.stub(this.client, 'say', (channel, message) => {
            expect(message).toEqual(__('customCommands.added'));
            done();
        });
        this.linkBlockerModule.handleMessage.apply(this, ["#channel", {}, "!command add test abc", this.client]);
    });

    it('tells me about wrong arguments', (done) => {
        sinon.stub(this.client, 'say', (channel, message) => {
            expect(message).toEqual(__('common.wrongArgumentNumber'));
            done();
        });
        this.linkBlockerModule.handleMessage.apply(this, ["#channel", {}, "!command add test abc 123", this.client]);
    });

    it('removes commands', (done) => {
        sinon.stub(this.client, 'say', (channel, message) => {
            expect(message).toEqual(__('customCommands.removed'));
            done();
        });

        new Command({
            name: 'test',
            text: 'test'
        }).save().then(() => {
            this.linkBlockerModule.handleMessage.apply(this, ["#channel", {}, "!command remove test", this.client]);
        });
    });

    it('tells me if i try to remove a command that does not exist', (done) => {
        let stub = sinon.stub(this.client, 'say', (channel, message) => {
            expect(message).toEqual(__('customCommands.notFound'));
            done();
        });
        this.linkBlockerModule.handleMessage.apply(this, ["#channel", {}, "!command remove test", this.client]);
    });

    it('prints the text of a command', (done) => {
        let stub = sinon.stub(this.client, 'say', (channel, message) => {
            expect(message).toEqual("test command!");
            done();
        });

        new Command({
            name: 'test',
            text: 'test command!'
        }).save().then(() => {
            this.linkBlockerModule.handleMessage.apply(this, ["#channel", {}, "!test", this.client]);
        });
    });
});