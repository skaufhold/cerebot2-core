
import sinon from 'sinon';
import { createExpect } from 'expect-async';
import tmi from 'tmi.js';
import { Configuration } from 'cerebot2-common';
import ConfigurationLoader from '../../src/helpers/ConfigurationLoader';

import LinkBlocker from '../../src/modules/LinkBlocker';

describe('link blocker', function() {
    beforeEach(() => {
        this.client = new tmi.client();
    });

    describe('with regular filtering', () => {
        beforeEach((done) => {
            this.config = new Configuration({
                _meta: { active: true },
                moderation: {
                    linksAllowed: false,
                    strictLinkFiltering: false,
                    defaultTimeout: 10
                }
            }).save().then(() => done());
        });

        it('blocks without protocol/3-part domain', (done) => {
            var expect = createExpect(done);
            var timeoutSpy = expect.spyOn(this.client, 'timeout');

            const linkBlockerModule = new LinkBlocker(this.client, new ConfigurationLoader());
            linkBlockerModule.handleMessage.apply(linkBlockerModule,
                [
                    "#channel",
                    {
                        'user-type': null,
                        username: 'testuser'
                    },
                    "hi www.google.com 123",
                    this.client,
                    () => {
                        expect(timeoutSpy).toHaveBeenCalledWith('#channel', 'testuser', 10);
                        done();
                    }
                ]);
        });

        it('blocks with protocol/3-part domain', (done) => {
            var expect = createExpect(done);
            var timeoutSpy = expect.spyOn(this.client, 'timeout');

            const linkBlockerModule = new LinkBlocker(this.client, new ConfigurationLoader());
            linkBlockerModule.handleMessage.apply(linkBlockerModule,
                [
                    "#channel",
                    {
                        'user-type': null,
                        username: 'testuser'
                    },
                    "hi http://www.google.com 123",
                    this.client,
                    () => {
                        expect(timeoutSpy).toHaveBeenCalledWith('#channel', 'testuser', 10);
                        done();
                    }
                ]);
        });

        it('does not block no protocol/2-part domain', (done) => {
            var expect = createExpect(done);
            var timeoutSpy = expect.spyOn(this.client, 'timeout');

            const linkBlockerModule = new LinkBlocker(this.client, new ConfigurationLoader());
            linkBlockerModule.handleMessage.apply(linkBlockerModule,
                [
                    "#channel",
                    {
                        'user-type': null,
                        username: 'testuser'
                    },
                    "hi google.com 123",
                    this.client,
                    () => {
                        expect(timeoutSpy).toNotHaveBeenCalled();
                        done();
                    }
                ]);
        });
    });

    describe('with strict filtering', () => {
        beforeEach((done) => {
            this.config = new Configuration({
                _meta: {active: true},
                moderation: {
                    linksAllowed: false,
                    strictLinkFiltering: true,
                    defaultTimeout: 10
                }
            }).save().then(() => done());
        });

        it('blocks without protocol/2-part domain', (done) => {
            var expect = createExpect(done);
            var timeoutSpy = expect.spyOn(this.client, 'timeout');

            const linkBlockerModule = new LinkBlocker(this.client, new ConfigurationLoader());
            linkBlockerModule.handleMessage.apply(linkBlockerModule,
                [
                    "#channel",
                    {
                        'user-type': null,
                        username: 'testuser'
                    },
                    "hi google.com 123",
                    this.client,
                    () => {
                        expect(timeoutSpy).toHaveBeenCalledWith('#channel', 'testuser', 10);
                        done();
                    }
                ]);
        });
    });
});