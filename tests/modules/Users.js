import sinon from 'sinon';
import expect from 'expect';

import { ChatUser } from 'cerebot2-common';

import UserCommands from '../../src/modules/Users';

describe('User commands', function() {
    beforeEach(() => {
        this.client = {
            say: function () {
            },
            on: function () {
            }
        };
        this.userModule = new UserCommands(this.client);
    });

    it('replies with last seen', (done) => {
        sinon.stub(this.client, 'say', (channel, message) => {
            expect(message).toEqual(__('users.lastSeen', {
                nick: 'test',
                when: 'an hour ago'
            }));
            done();
        });
        new ChatUser({
            nick: 'test',
            lastSeen: Date.now() - 60*60*1000
        }).save().then(() => {
            this.userModule.handleMessage.apply(this, ["#channel", {}, "!user test", this.client]);
        });
    });

    it('replies with not found if user does not exist', (done) => {
        sinon.stub(this.client, 'say', (channel, message) => {
            expect(message).toEqual(__('users.notFound'));
            done();
        });
        this.userModule.handleMessage.apply(this, ["#channel", {}, "!user test", this.client]);
    });
});