import sinon from 'sinon';
import sinonMocha from 'sinon-mocha';
sinonMocha.enhance(sinon);
import { connect, mongoose } from 'cerebot2-common';
import '../src/i18n-config';

const environment = require("../environment.test.json");
connect(environment.mongo);

function clearCollections() {
    return new Promise((resolve, reject) => {
        mongoose.connection.db.collections(function(err, collections) {
            const promises = collections.map((coll) => {
                new Promise((done, fail) => {
                    if (coll.s.name.startsWith('system')) {
                        return done();
                    } else {
                        coll.remove(() => {
                            done();
                        });
                    }
                });
            });
            Promise.all(promises).then(() => resolve(), reject)
        });
    });
}

before(function(done) {
    if (mongoose.connection.readyState != 1) {
        mongoose.connection.on('connected', () => {
            clearCollections().then(done);
        });
    } else {
        clearCollections().then(done);
    }
});

afterEach(function(done)  {
    clearCollections().then(done);
});
