"use strict";

var plan = require('flightplan');

plan.target('production', [
    {
        host: 'ceredev.eu',
        username: 'cerebot2',
        agent: process.env.SSH_AUTH_SOCK
    }
]);

const app = 'cerebot2-chat';
const tmpDir = app + '-' + new Date().getTime();
const buildDir = `/home/cerebot2/${app}/builds`;
const currentBuildDir = `/home/cerebot2/${app}/builds/${tmpDir}`;
const activeBuildDir = `/home/cerebot2/${app}/current`;

// run commands on localhost
plan.local(function(local) {
    local.log('Copy files to remote hosts');
    var filesToCopy = local.exec('git ls-files', {silent: true});
    // rsync files to all the target's remote hosts
    local.transfer(filesToCopy, `/tmp/${tmpDir}`);
});

// run commands on the target's remote hosts
plan.remote(function(remote) {
    remote.log('Move folder to web root');
    remote.exec(`mkdir -p ${buildDir} && cp -R /tmp/${tmpDir} ${buildDir}`);
    remote.rm(`-rf /tmp/${tmpDir}`);

    remote.log('Install dependencies');
    remote.exec(`cd ${currentBuildDir} && npm --production install`, {silent: true});
    remote.exec(`cd ${currentBuildDir} && npm run build`);

    remote.exec(`ln -snf ${currentBuildDir} ${activeBuildDir}`);

    remote.log('Reload application');
    remote.exec(`sudo stop ${app}`, {failsafe: true});
    remote.exec(`sudo start ${app}`);
});
