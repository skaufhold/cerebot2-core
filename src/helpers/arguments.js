import minimist from 'minimist';
import { parse as parseQuotes } from 'shell-quote';

export function parseCommand(...args) {
    if (args.length == 2) {
        const [command, input] = args;
        if (input.indexOf(`!${command}`) < 0) return null;
        return minimist(parseQuotes(input).slice(1));
    } else if (args.length == 1) {
        const [input] = args;
        return minimist(parseQuotes(input));
    }
}
