
import { Configuration } from 'cerebot2-common';

export default class ConfigurationLoader {
    constructor() {
        this.config = null;
        this.loading = false;
        this.loadCallbacks = [];
    }

    reload() {
        this.loading = true;
        Configuration.findOne({'_meta.active': true}).then((config) => {
            this.loading = false;
            this.config = config;
            this.configDidLoad();
        }, err => this.configFailedToLoad(err));
    }

    configDidLoad() {
        this.loadCallbacks.forEach(cb =>  {
            if (cb[0]) cb[0]();
        });
    }

    configFailedToLoad() {
        this.loadCallbacks.forEach(cb =>  {
            if (cb[1]) cb[1](err);
        });
    }

    get(path) {
        return new Promise((resolve, reject) => {
            if (!this.loading && this.config) {
                resolve(this.config.get(path));
            } else {
                this.loadCallbacks.push([() => {
                    resolve(this.config.get(path));
                }, (err) => {
                    reject(err);
                }]);

                if (!this.loading) this.reload();
            }
        });
    }
}