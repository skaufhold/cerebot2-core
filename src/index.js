import fs from 'fs';
import tmi from 'tmi.js'
import http from 'http';
import { Client as RestClient } from 'node-rest-client';
import environment from './environment-loader';
import { Configuration, connect } from 'cerebot2-common';
import ConfigurationLoader from './helpers/ConfigurationLoader';
import './i18n-config';

connect(environment.mongo);

Configuration.find({'_meta.active': true}, function (err, configs) {
    if (err) {
        console.error(err);
        process.exit(1);
    }
    if (configs.length > 1) console.warn("Found more than one active configuration!");

    var config = null;

    if (configs.length == 0) {
        console.error("No configurations found, using default");
        var defaultConfigs = require("../database-bootstrap.json").Configuration;

        defaultConfigs.forEach((c) => new Configuration(c).save());

        config = defaultConfigs[0];
    }
    else {
        config = configs[0];
    }

    var client = new tmi.client({
        options: {
            debug: true
        },
        connection: {
            server: "192.16.64.180",
            port: 80,
            reconnect: true
        },
        identity: {
            username: environment.credentials.nick,
            password: "oauth:" + environment.credentials.oauth_token
        },
        channels: config.channels.map(c => "#"+c)
    });

    client.connect();

    const configurationLoader = new ConfigurationLoader();

    console.log("Loading bot modules...");
    // load all modules and call them with the client object
    fs.readdirSync(__dirname + '/modules').forEach(function (file) {
        console.log("Loading " + file);
        var module = require(__dirname + '/modules/' + file).default;
        if (~file.indexOf('.js')) new module(client, configurationLoader);
    });
});