import dotenv from 'dotenv';
dotenv.config();

let localEnv = {};

try {
    localEnv = (process.env.NODE_ENV == 'test') ? require("../environment.test.json") : require("../environment.json")
}
catch(e) {}

const env = Object.assign({
    "mongo": process.env.MONGO_URL,
    "credentials": {
        "nick": process.env.TWITCH_CHAT_NICK,
        "oauth_token": process.env.TWITCH_CHAT_TOKEN
    },
    "twitch": {
        "clientID": process.env.TWITCH_CLIENTID,
        "clientSecret": process.env.TWITCH_SECRET
    }
}, localEnv);

export default env;
