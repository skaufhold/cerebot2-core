import { ChatUser, Message } from 'cerebot2-common';

export default class Logger {
    constructor(client) {
        this.client = client;
        client.on('chat', (...args) => this.handleMessage.apply(this, args));
    }

    /**
     * Handles the message received event
     */
    handleMessage(channel, user, message, self) {
        ChatUser.findOne({nick: user.username}).then((chatUser) => {
            if (chatUser === null) {
                ChatUser.create({
                    nick: user.username
                }, (err, newUser) => {
                    if (!err) {
                        this.logMessage(channel, newUser, message);
                    } else {
                        console.warn(err);
                    }
                });
            } else {
                this.logMessage(channel, chatUser, message);
            }
        });
    }

    /**
     * Saves a message log in the data store
     * @param channel Channel name
     * @param chatUser chatUser model object
     * @param message message body
     * @returns {Object|*}
     */
    logMessage(channel, chatUser, message) {
        return Message.create({
            body: message,
            channelName: channel,
            nick: chatUser.nick,
            chatUser: chatUser
        });
    }
}