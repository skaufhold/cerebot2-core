import { parseCommand } from '../helpers/arguments';
import { ChatUser } from 'cerebot2-common';
import moment from 'moment';

export default class Users {
    constructor(client) {
        this.client = client;
        client.on('chat', (...args) => this.handleMessage.apply(this, args));
    }

    handleMessage(channel, user, message, self) {
        var argv = parseCommand("user", message);
        if (argv) {
            ChatUser.findOne({nick: argv._[0]}).then(
                chatUser => {
                    if (chatUser) {
                        this.client.say(channel, __('users.lastSeen', {
                            nick: chatUser.nick,
                            when: moment(chatUser.lastSeen).fromNow()
                        }));
                    } else {
                        this.client.say(channel, __('users.notFound'));
                    }
                }
            );
        }
    }
}