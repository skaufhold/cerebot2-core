import { ChatUser } from 'cerebot2-common';

const LINK_REGEX = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/i;
const DOMAIN_REGEX = /(\s|^)(?:[a-z\-0-9]+\.)?[a-z\-0-9]{4,}\.[a-z]{2,3}(\s|$)/i;

export default class LinkBlocker {
    constructor(client, config) {
        this.client = client;
        this.config = config;
        client.on('chat', (...args) => this.handleMessage.apply(this, args));
    }

    handleMessage(channel, user, message, self, done) {
        this.config.get('moderation').then(moderation => {
            if (moderation.linksAllowed || user['user-type']) {
                return;
            }
            if (message.match(LINK_REGEX) || (moderation.strictLinkFiltering && message.match(DOMAIN_REGEX))) {
                this.client.timeout(channel, user.username, moderation.defaultTimeout);
            }
            if (done) done();
        });
    }
}
