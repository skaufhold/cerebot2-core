import { parseCommand } from '../helpers/arguments';
import { Command } from 'cerebot2-common';
import { format } from 'util';

export default class CustomCommands {
    constructor(client) {
        this.client = client;
        client.on('chat', (...args) => this.handleMessage.apply(this, args));
    }

    handleMessage(channel, user, message, self) {
        var argv = parseCommand("command", message);
        if (argv) {
            var action = argv._[0];
            if (action == "add") {
                if (argv._.length != 3)  {
                    this.client.say(channel, __('common.wrongArgumentNumber'));
                } else {
                    var name = argv._[1],
                        text = argv._[2];
                    new Command({
                        name: name,
                        text: text
                    }).save((err) => {
                        if (!err) this.client.say(channel, __('customCommands.added'));
                    });
                }
            } else if (action == "remove") {
                if (argv._.length != 2)  {
                    this.client.say(__('common.wrongArgumentNumber'));
                } else {
                    Command.findOne({name: argv._[1]}, (err, command) => {
                        if (err || command === null) {
                            this.client.say(channel, __('customCommands.notFound'));
                        } else {
                            command.remove((err2) => {
                                if (!err2) this.client.say(channel, __('customCommands.removed'));
                            });
                        }
                    });
                }
            }
        }
        else {
            var match = message.match(/^!([A-Za-z]+)/);
            if (match) {
                let args = parseCommand(message)._.slice(1);
                Command.findOne({name: match[1]}, (err, command) => {
                    if (!err && command !== null) {
                        this.client.say(channel, format.apply(null, [command.text].concat(args)));
                    }
                });
            }
        }
    }
}
