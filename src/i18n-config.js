import i18n, { __ } from 'i18n';
import path from "path";

i18n.configure({
    locales: ['en'],
    directory: path.resolve(__dirname + '/../locales'),
    objectNotation: true
});

global.i18n = i18n;
global.__ = i18n.__;